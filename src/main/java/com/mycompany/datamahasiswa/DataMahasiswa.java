/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.datamahasiswa;

/**
 *
 * @author Ariawan
 */
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DataMahasiswa {

    public static void main(String[] args) {
        List<Mahasiswa> mahasiswaList = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Aplikasi Manajemen Data Mahasiswa");
            System.out.println("1. Tambah Mahasiswa");
            System.out.println("2. Hapus Mahasiswa");
            System.out.println("3. Tampilkan Data Mahasiswa");
            System.out.println("4. Keluar");
            System.out.print("Pilih menu (1/2/3/4): ");

            int pilihan = scanner.nextInt();
            scanner.nextLine(); 

            switch (pilihan) {
                case 1:
                    tambahMahasiswa(mahasiswaList, scanner);
                    break;
                case 2:
                    hapusMahasiswa(mahasiswaList, scanner);
                    break;
                case 3:
                    tampilkanDataMahasiswa(mahasiswaList);
                    break;
                case 4:
                    System.out.println("Terima kasih! Keluar dari aplikasi.");
                    System.exit(0);
                default:
                    System.out.println("Pilihan tidak valid.");
            }
        }
    }

    private static void tambahMahasiswa(List<Mahasiswa> mahasiswaList, Scanner scanner) {
        System.out.print("Masukkan NIM (int): ");
        int nim = scanner.nextInt();
        scanner.nextLine(); // Membersihkan newline

        System.out.print("Masukkan Nama (String): ");
        String nama = scanner.nextLine();

        System.out.print("Masukkan Nomor HP Utama (Array): ");
        String nomorHpUtama = scanner.nextLine();

        System.out.print("Masukkan Nomor HP Orang Tua (Array): ");
        String[] nomorHpOrangTua = scanner.nextLine().split(",");

        System.out.print("Masukkan IPK (double): ");
        double ipk = scanner.nextDouble();

        System.out.print("Masukkan Berat Badan (double): ");
        double beratBadan = scanner.nextDouble();

        System.out.print("Masukkan Tinggi Badan (int): ");
        int tinggiBadan = scanner.nextInt();

        System.out.print("Masukkan Jenis Kelamin (char, L/P): ");
        char jenisKelamin = scanner.next().charAt(0);

        System.out.print("Apakah Mahasiswa Aktif (boolean, true/false): ");
        boolean isAktif = scanner.nextBoolean();

        System.out.print("Masukkan Umur (byte): ");
        byte umur = scanner.nextByte();

        System.out.print("Masukkan Total SKS (short): ");
        short totalSks = scanner.nextShort();

        Mahasiswa mahasiswa = new Mahasiswa(nim, nama, nomorHpUtama, nomorHpOrangTua, ipk, beratBadan, tinggiBadan, jenisKelamin, isAktif, umur, totalSks);
        mahasiswaList.add(mahasiswa);

        System.out.println("Mahasiswa berhasil ditambahkan.");
    }

    private static void hapusMahasiswa(List<Mahasiswa> mahasiswaList, Scanner scanner) {
        System.out.print("Masukkan NIM mahasiswa yang akan dihapus: ");
        int nimHapus = scanner.nextInt();

        Mahasiswa mahasiswaHapus = null;
        for (Mahasiswa mahasiswa : mahasiswaList) {
            if (mahasiswa.getNim() == nimHapus) {
                mahasiswaHapus = mahasiswa;
                break;
            }
        }

        if (mahasiswaHapus != null) {
            mahasiswaList.remove(mahasiswaHapus);
            System.out.println("Mahasiswa dengan NIM " + nimHapus + " telah dihapus.");
        } else {
            System.out.println("Mahasiswa dengan NIM " + nimHapus + " tidak ditemukan.");
        }
    }

    private static void tampilkanDataMahasiswa(List<Mahasiswa> mahasiswaList) {
        System.out.println("Daftar Mahasiswa:");
        for (Mahasiswa mahasiswa : mahasiswaList) {
            System.out.println("NIM: " + mahasiswa.getNim() + ", Nama: " + mahasiswa.getNama() +
                    ", Nomor HP Utama: " + mahasiswa.getNomorHpUtama() +
                    ", Nomor HP Orang Tua: " + String.join(", ", mahasiswa.getNomorHpOrangTua()) +
                    ", IPK: " + mahasiswa.getIpk() +
                    ", Berat Badan (kg): " + mahasiswa.getBeratBadan() +
                    ", Tinggi Badan (cm): " + mahasiswa.getTinggiBadan() +
                    ", Jenis Kelamin: " + mahasiswa.getJenisKelamin() +
                    ", Aktif: " + mahasiswa.isAktif() +
                    ", Umur: " + mahasiswa.getUmur() +
                    ", Total SKS: " + mahasiswa.getTotalSks());
        }
    }

    public static class Mahasiswa {
        // Tipe data yang digunakan
        private int nim;
        private String nama;
        private String nomorHpUtama;
        private String[] nomorHpOrangTua;
        private double ipk;
        private double beratBadan;
        private int tinggiBadan;
        private char jenisKelamin;
        private boolean isAktif;
        private byte umur;
        private short totalSks;

        public Mahasiswa(int nim, String nama, String nomorHpUtama, String[] nomorHpOrangTua,
                         double ipk, double beratBadan, int tinggiBadan, char jenisKelamin,
                         boolean isAktif, byte umur, short totalSks) {
            this.nim = nim;
            this.nama = nama;
            this.nomorHpUtama = nomorHpUtama;
            this.nomorHpOrangTua = nomorHpOrangTua;
            this.ipk = ipk;
            this.beratBadan = beratBadan;
            this.tinggiBadan = tinggiBadan;
            this.jenisKelamin = jenisKelamin;
            this.isAktif = isAktif;
            this.umur = umur;
            this.totalSks = totalSks;
        }

        public int getNim() {
            return nim;
        }

        public String getNama() {
            return nama;
        }

        public String getNomorHpUtama() {
            return nomorHpUtama;
        }

        public String[] getNomorHpOrangTua() {
            return nomorHpOrangTua;
        }

        public double getIpk() {
            return ipk;
        }

        public double getBeratBadan() {
            return beratBadan;
        }

        public int getTinggiBadan() {
            return tinggiBadan;
        }

        public char getJenisKelamin() {
            return jenisKelamin;
        }

        public boolean isAktif() {
            return isAktif;
        }

        public byte getUmur() {
            return umur;
        }

        public short getTotalSks() {
            return totalSks;
        }
    }
}
